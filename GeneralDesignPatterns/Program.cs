﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            IArayuz kamera = Arayuz_Fabrikasi.getArayuz(Arayuz_Tipleri.KAMERA, "3.0.3");
            Seriport.Lazy_Instance.ObserverEkle((Observer)kamera);

            IArayuz harita = Arayuz_Fabrikasi.getArayuz(Arayuz_Tipleri.HARITA, "1.0.3");
            Seriport.Lazy_Instance.ObserverEkle((Observer)harita);

            Seriport.Lazy_Instance.Seri_Porttan_Veri_Geldi_Arayuzlere_Veri_Gonder();

            Console.ReadLine();
        }
    }
}
