﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralDesignPatterns
{
    interface IArayuz
    {
        String getVersion();
        String getName();
    }
}
