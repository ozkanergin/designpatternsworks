﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralDesignPatterns
{
    public class Harita : Observer,IArayuz
    {
        private Arayuz_Tipleri name;
        private String version;


        public string getName()
        {
            return name.ToString();
        }

        public string getVersion()
        {
            return version;
        }

        public Harita(Arayuz_Tipleri name, string version)
        {
            this.name = name;
            this.version = version;
        }
        public override void Update()
        {
            Console.WriteLine(getName() + " veri gonderirken hata.");
        }
    }
}
