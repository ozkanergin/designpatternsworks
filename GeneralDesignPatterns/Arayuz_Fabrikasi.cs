﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralDesignPatterns
{
    class Arayuz_Fabrikasi
    {
        public static IArayuz getArayuz(Arayuz_Tipleri name, String version)
        {
            IArayuz arayuz;
            if (name == Arayuz_Tipleri.KAMERA)
            {
                arayuz = new Kamera(name,version);
            }
            else if (name == Arayuz_Tipleri.ARAYUZ)
            {
                arayuz = new Arayuz(name, version);
            }
            else if (name == Arayuz_Tipleri.HARITA)
            {
                arayuz = new Harita(name, version);
            }
            else
            {
                throw new Exception("Baykar yetkilileri ile iletisime geciniz");
            }
            return arayuz;
        }
    }
}
