﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralDesignPatterns
{
    public class Seriport
    {
        static object lock_object = new object();
        private static Seriport instance = null;

        //public static Seriport Instance
        //{
        //    get
        //    {
        //        lock (lock_object)
        //        {
        //            if (instance == null)
        //            {
        //                instance = new Seriport();
        //            }
        //            return instance;
        //        }
        //    }
        //    private set { instance = value; }
        //}
        
        private static readonly Lazy<Seriport> lazy = new Lazy<Seriport>(() => new Seriport(),true);
        public static Seriport Lazy_Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        private List<Observer> list_observer;

        private Seriport()
        {
            list_observer = new List<Observer>();
        }

        public void ObserverEkle(Observer observer)
        {
            list_observer.Add(observer);
        }

        public void ObserverCikar(Observer observer)
        {
            list_observer.Remove(observer);
        }

        public void Notify()
        {
            foreach (Observer item in list_observer)
            {
                item.Update();
            }
        }

        public void Seri_Porttan_Veri_Geldi_Arayuzlere_Veri_Gonder()
        {
            Notify();
        }
    }
}
