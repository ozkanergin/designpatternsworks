﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryTelefon
{
    public class Note8 : ITelefon
    {

        private String model;
        private String batarya;
        private int en;
        private int boy;

        public Note8(String model, String batarya, int en, int boy)
        {
            this.model = model;
            this.batarya = batarya;
            this.en = en;
            this.boy = boy;
        }

        public string getBatarya()
        {
            return batarya;
        }

        public int getBoy()
        {
            return boy;
        }

        public int getEn()
        {
            return en;
        }

        public string getModel()
        {
            return model;
        }

        public override string ToString()
        {
            return "Note8{" +
                            "model='" + model + '\'' +
                            ", batarya='" + batarya + '\'' +
                            ", en=" + en +
                            ", boy=" + boy +
                            '}';
        }

        public void Kalemle_Ciz()
        {
            Console.WriteLine("Kalemle ciziyoruum.");
        }
    }
}
