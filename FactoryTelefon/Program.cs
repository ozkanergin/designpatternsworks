﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryTelefon
{
    class Program
    {
        static void Main(string[] args)
        {
            ITelefon s8 = TelefonFabrikasi.getTelefon("s8", "2600mah", 4, 7);

            ITelefon note8 = TelefonFabrikasi.getTelefon("note8", "3000mah", 5, 8);

            Console.WriteLine("S8 için telefon özellikleri: ");
            Console.WriteLine(s8);

            Console.WriteLine("****************");


            Console.WriteLine("Note8 için telefon özellikleri: ");
            Console.WriteLine(note8);
            ((Note8)note8).Kalemle_Ciz();
            Console.ReadLine();
        }
    }
}
