﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryTelefon
{
    public class TelefonFabrikasi
    {
        public static ITelefon getTelefon(String model, String batarya, int en, int boy)
        {
            ITelefon telefon;
            if (string.Equals(model, "S8", StringComparison.OrdinalIgnoreCase))
            {
                telefon = new S8(model, batarya, en, boy);
            }
            else if (string.Equals(model, "Note8", StringComparison.OrdinalIgnoreCase))
            {
                telefon = new Note8(model, batarya, en, boy);
            }
            else
            {
                throw new Exception("Geçerli bir model değildir!");
            }
            return telefon;
        }
    }
}
