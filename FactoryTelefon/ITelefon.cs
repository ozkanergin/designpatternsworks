﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryTelefon
{
   public interface ITelefon
   {
       String getModel();
       String getBatarya();
       int getEn();
       int getBoy();
   }
}
