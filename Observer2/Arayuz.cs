﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer2
{
    public class Arayuz : Observer
    {
        public override void Update()
        {
            Console.WriteLine("Istemci arayuzlerine veri gonderildi.");
        }
    }
}
