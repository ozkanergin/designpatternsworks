﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer2
{
    public enum Tanimli_Prosesler
    {
        KAMERA,
        ARAYUZ,
        HARITA
    }

    class Program
    {
        static void Main(string[] args)
        {
            Harita harita = new Harita();
            Kamera kamera = new Kamera();
            Arayuz arayuz = new Arayuz();
            Seriport.Lazy_Instance.ObserverEkle(kamera);
            Seriport.Lazy_Instance.ObserverEkle(arayuz);
            Seriport.Lazy_Instance.ObserverEkle(harita);

            //Seriport.Instance.Seri_Porttan_Veri_Geldi_Arayuzlere_Veri_Gonder();
            Seriport.Lazy_Instance.Seri_Porttan_Veri_Geldi_Arayuzlere_Veri_Gonder();


            #region SINGLETON BOMBER
            //Seriport.DerivedSingleton bomb = new Seriport.DerivedSingleton();
            //bomb.Seri_Porttan_Veri_Geldi_Arayuzlere_Veri_Gonder();
            #endregion

            Console.ReadLine();



        }
    }
}
