﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    public class IMEYT : Observer
    {
        public override void Update()
        {
            Console.WriteLine("Ahmet abi son dakika paket geldigi icin sinirlendi.");
            Console.WriteLine("Paket degisiklikleri gecildi.");
        }
    }
}
