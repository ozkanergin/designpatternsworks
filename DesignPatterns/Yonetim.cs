﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    public class Yonetim
    {
        static object lock_object = new object();
        private static Yonetim instance = null;

        public static Yonetim Instance
        {
            get
            {
                lock (lock_object)
                {
                    if(instance == null)
                    {
                        instance = new Yonetim();
                    }
                    return instance;
                }
            }
        }

        private List<Observer> list_observer;

        private Yonetim()
        {
            list_observer = new List<Observer>();
        }

        public void ObserverEkle(Observer observer)
        {
            list_observer.Add(observer);
        }

        public void ObserverCikar(Observer observer)
        {
            list_observer.Remove(observer);
        }

        public void Notify()
        {
            foreach (Observer item in list_observer)
            {
                item.Update();
            }
        }

        public void HA_Icin_Guncelleme_Talep_Et()
        {
            Notify();
        }
    }
}
