﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    public class Gomulu : Observer
    {
        public override void Update()
        {
            Console.WriteLine("MDT yazildi.");
            Console.WriteLine("Paket exceli olusturuldu.");
        }
    }
}
