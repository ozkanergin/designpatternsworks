﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Gomulu gomulu = new Gomulu();
            IMEYT imeyt = new IMEYT();
            Yonetim.Instance.ObserverEkle(gomulu);
            Yonetim.Instance.ObserverEkle(imeyt);
            Yonetim.Instance.HA_Icin_Guncelleme_Talep_Et();
            Console.ReadLine();
        }
    }
}
