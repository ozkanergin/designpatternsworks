﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lazy
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();

            var ben = new Ozkan();

            #region LAZY
            //var kendim = new Lazy<Ozkan>();
            #endregion

            sw.Stop();

            Console.WriteLine("Gecen sure : " + sw.ElapsedMilliseconds.ToString());

            sw.Start();


            Console.ReadLine();
        }
    }
}
